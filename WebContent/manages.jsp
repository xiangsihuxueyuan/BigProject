<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>NULL</title>
</head>

        <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="css/login.css" />
		<script src="http://www.jq22.com/jquery/jquery-1.10.2.js"></script>
		<script src="bootstrap/js/bootstrap.min.js"></script>
    	<link rel="stylesheet" type="text/css" href="css/bootstrap-combined.min.css"/>
	    <link rel="stylesheet" type="text/css" href="css/bootstrap-responsive.css"/>
	    <link rel="stylesheet" type="text/css" href="css/bootstrap-responsive.min.css"/>
	    <link rel="stylesheet" type="text/css" href="css/bootstrap.css"/>
	    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css"/>
	    <link rel="stylesheet" type="text/css" href="css/editor.css"/>
	    <link rel="stylesheet" type="text/css" href="css/layoutit.css"/>
		
		<script>
			$(document).ready(function() {
				
				$("#Login_start_").click(function() {
					$("#regist_container").hide();
					$("#_close").show();
					$("#_start").animate({
						left: '350px',
						height: '520px',
						width: '400px'
					}, 500, function() {
						$("#login_container").show(500);
						$("#_close").animate({
							height: '40px',
							width: '40px'
						}, 500);
					});
				});
		
				$("#Regist_start_").click(function() {
					$("#login_container").hide();
					$("#_close").show();
					$("#_start").animate({
						left: '350px',
						height: '520px',
						width: '400px'
					}, 500, function() {
						$("#regist_container").show(500);
						$("#_close").animate({
							height: '40px',
							width: '40px'
						}, 500);
					});
				});
	
				$("#_close").click(function() {
					
					$("#_close").animate({
						height: '0px',
						width: '0px'
					}, 500, function() {
						$("#_close").hide(500);
						$("#login_container").hide(500);
						$("#regist_container").hide(500);
						$("#_start").animate({
							left: '0px',
							height: '0px',
							width: '0px'
						}, 500);
					});
				});
			
				$("#toRegist").click(function(){
					$("#login_container").hide(500);
					$("#regist_container").show(500);
				});
				
				$("#toLogin").click(function(){
					$("#regist_container").hide(500);
					$("#login_container").show(500);
				});
			});
		</script>
		
		<style>
			th{
				background-color: khaki;
				color: white;
			}
	
		</style>
	</head>

	<body style="background-color: #000000;">
      <div id="mya">
		<a id="Login_start_" class="btn btn-danger" style="width:100px;height:40px;border-radius: 0;margin-left: 20px;">添加</a>
		<a id="Regist_start_" class="btn btn-success" style="width:100px;height:40px;border-radius: 0;margin-left: 10px">更改</a>
           
</div>

		<div id='_start'>
			<div id='_close' style="display: none;">
				<span class="glyphicon glyphicon-remove"></span>
			</div>
			<br /> 
			
			<div id="login_container">
				<div id="lab1">
					<span id="lab_login">添加图书管理员</span>
					
				</div>
				<div style="width:330px;">
					<span id="lab_type1"></span>
				</div>
				
				<form action="AdminInsertManage">
			<div id="form_container2" style="padding-top: 25px;">
					
					<input type="text" class="form-control"   placeholder="职工号" id="regist_account" name="m_num"/>
					<input type="text" class="form-control" placeholder="姓名"  id="regist_password1" name="m_name"/>
					<input type="text" class="form-control" placeholder="性别"  id="regist_password2" name="m_sex"/>
				</div>

				<div style="width:330px;height:100px;border-bottom: 1px solid #FFFFFF;">
					
					<input type="submit" value="确认添加" class="btn btn-danger" id="regist_btn" />
				</div>
				</form>
				
			</div>
			
			
			
			<div id='regist_container' style="display: none;">
				<div id="lab1">
					<span id="lab_login">修改图书管理员</span>
					
				</div>
				
				
				<form action="AdminUdateManage" method="get">
				
				
				<div id="form_container2" style="padding-top: 25px;">
					
					<input type="text" class="form-control"   placeholder="职工号" id="regist_account"    name="m_num"/>
					<input type="text" class="form-control" placeholder="姓名"    id="regist_password1" name="m_name"/>
					<input type="text" class="form-control" placeholder="性别"    id="regist_password2"  name="m_sex"/>
			

				</div>
				<input type="submit" value="确认更改" class="btn btn-success" id="regist_btn" />
				</form>
				
				
				
				
			
			</div>
		</div>



        	<div class="container-fluid">
	<div class="row-fluid">
		<div class="span12">
			<div class="carousel slide" id="carousel-257569">
	
		
			
				<form class="form-search form-inline"  action="AdminManageList"  method="get">
				
			<table class="table table-striped">
				<thead>
				
					<tr>
						<th>
							编号
						</th>
						<th>
							姓名
						</th>
						<th>
							注册时间
						</th>
						<th>
							性别
						</th>
						<th>
							操作
						</th>
					</tr>
					
				</thead>
				
				<c:forEach items="${list}" var="p">
				<tbody>
					<tr>
						<td>
							${p.m_num}
						</td>
						<td>
							${p.m_name}
						</td>
						<td>
							01/04/2012
						</td>
						<td>
							${p.m_sex}
						</td>
						<td>
							
							<a href="AdminDelectManage?m_num=${p.m_num}">删除</a>
							
						</td>
					</tr>		
				</tbody>
			</c:forEach>
			
			</table> 
			</form>
			<a href="DemoAdmin.jsp"><button class="btn btn-block btn-primary" type="button">退出</button></a>
		</div>
		</div>
	</div>
</div>
	
	<script type="text/javascript">
		var clock = '';
		var nums = 30;
		var btn;
		function sendCode(thisBtn) {
			btn = thisBtn;
			btn.disabled = true; 
			btn.value = '重新获取('+nums+')';
			clock = setInterval(doLoop, 1000);
		}

		function doLoop() {
			nums--;
			if (nums > 0) {
				btn.value = '重新获取('+nums+')';
			} else {
				clearInterval(clock); 
				btn.disabled = false;
				btn.value = '点击发送验证码';
				nums = 10; 
			}
		}
		
		$(document).ready(function(){
			$("#login_QQ").click(function(){
				alert("暂停使用！");
			});
			$("#login_WB").click(function(){
				alert("暂停使用！");
			});
		});
	</script>
	
	<script type="text/javascript" src="js/bootstrap.js" ></script>
	<script src="html5plus://ready"></script>
	<script type="text/javascript" src="js/bootstrap.min.js" ></script>
	<script type="text/javascript" src="js/ckeditor.js" ></script>
	<script type="text/javascript" src="js/h.js" ></script>
	<script type="text/javascript" src="js/config.js" ></script>
	<script type="text/javascript" src="js/jquery-2.0.0.min.js" ></script>
	<script type="text/javascript" src="js/jquery-ui.js" ></script>
	<script type="text/javascript" src="js/jquery.htmlClean.js" ></script>
	<script type="text/javascript" src="js/jquery.min.js" ></script>
	<script type="text/javascript" src="js/jquery.ui.touch-punch.min.js" ></script>
	<script type="text/javascript" src="js/scripts.js" ></script>
	<script type="text/javascript" src="js/styles.js" ></script>
	<script type="text/javascript" src="js/zh-cn.js" ></script>
</body>
</html>