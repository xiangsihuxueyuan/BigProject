package com.Manages.entity;

public class Manage {
    private int m_num;
    private String m_name;
    private String m_sex;
	
    public Manage() {
		super();
	}

	public Manage(int m_num, String m_name, String m_sex) {
		super();
		this.m_num = m_num;
		this.m_name = m_name;
		this.m_sex = m_sex;
	}

	public int getM_num() {
		return m_num;
	}

	public void setM_num(int m_num) {
		this.m_num = m_num;
	}

	public String getM_name() {
		return m_name;
	}

	public void setM_name(String m_name) {
		this.m_name = m_name;
	}

	public String getM_sex() {
		return m_sex;
	}

	public void setM_sex(String m_sex) {
		this.m_sex = m_sex;
	}

	@Override
	public String toString() {
		return "Manage [m_num=" + m_num + ", m_name=" + m_name + ", m_sex=" + m_sex + "]";
	}
    
    
    
    
    
    
    
    
    
}
