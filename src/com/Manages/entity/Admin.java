package com.Manages.entity;

public class Admin {
       private int a_num;
       private int a_password;
	
       
       
       public Admin() {
		super();
	}



	public Admin(int a_num, int a_password) {
		super();
		this.a_num = a_num;
		this.a_password = a_password;
	}



	public int getA_num() {
		return a_num;
	}



	public void setA_num(int a_num) {
		this.a_num = a_num;
	}



	public int getA_password() {
		return a_password;
	}



	public void setA_password(int a_password) {
		this.a_password = a_password;
	}



	@Override
	public String toString() {
		return "Admin [a_num=" + a_num + ", a_password=" + a_password + "]";
	}
       
       
       
       
       
       
       
       
}
