package com.Manages.entity;

public class Users {
    private String user_name;
    private String user_password;
    private int iden;
    private int s_numb;
    private int m_numb;
    
	public Users() {
		super();
	}
	
	public Users(String user_name, String user_password) {
		super();
		this.user_name = user_name;
		this.user_password = user_password;
		
	}
	
	public Users(String user_name, String user_password, int iden, int s_numb, int m_numb) {
		super();
		this.user_name = user_name;
		this.user_password = user_password;
		this.iden = iden;
		this.s_numb = s_numb;
		this.m_numb = m_numb;
	}
	public String getUser_name() {
		return user_name;
	}
	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}
	public String getUser_password() {
		return user_password;
	}
	public void setUser_password(String user_password) {
		this.user_password = user_password;
	}
	public int getIden() {
		return iden;
	}
	public void setIden(int iden) {
		this.iden = iden;
	}
	public int getS_numb() {
		return s_numb;
	}
	public void setS_numb(int s_numb) {
		this.s_numb = s_numb;
	}
	public int getM_numb() {
		return m_numb;
	}
	public void setM_numb(int m_numb) {
		this.m_numb = m_numb;
	}
	
	@Override
	public String toString() {
		return "Users [user_name=" + user_name + ", user_password=" + user_password + ", iden=" + iden + ", s_numb="
				+ s_numb + ", m_numb=" + m_numb + "]";
	}

     








} 
