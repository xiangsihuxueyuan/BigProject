package com.Manages.entity;

public class Student {
                private String s_name;
                private String s_sex;
                private int s_numb;
                private int s_phone;
				
                public Student() {
					super();
				}

                              
  			public Student(String s_name, String s_sex, int s_numb, int s_phone) {
					super();
					this.s_name = s_name;
					this.s_sex = s_sex;
					this.s_numb = s_numb;
					this.s_phone = s_phone;
				}



				public String getS_name() {
					return s_name;
				}

				public void setS_name(String s_name) {
					this.s_name = s_name;
				}

				public String getS_sex() {
					return s_sex;
				}

				public void setS_sex(String s_sex) {
					this.s_sex = s_sex;
				}

				public int getS_numb() {
					return s_numb;
				}

				public void setS_numb(int s_numb) {
					this.s_numb = s_numb;
				}

				public int getS_phone() {
					return s_phone;
				}

				public void setS_phone(int s_phone) {
					this.s_phone = s_phone;
				}


				@Override
				public String toString() {
					return "Student [s_name=" + s_name + ", s_sex=" + s_sex + ", s_numb=" + s_numb + ", S_phone="
							+ s_phone + "]";
				}
                
                
                
                
}
