package com.Manages.dao.Admin;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.Manages.dao.BaseDao;
import com.Manages.entity.Admin;
import com.Manages.entity.Manage;
import com.Manages.entity.Student;
import com.Manages.entity.Users;






public class AdminImpl {
                    
	public Admin findAminByNum(Admin admin){
		
		
		try {
			Connection con=BaseDao.getCon();
			PreparedStatement pstm=con.prepareStatement("select * from admin where a_num=? and a_password=?");
		    pstm.setInt(1, admin.getA_num());
		    pstm.setInt(2, admin.getA_password());
			ResultSet rs=pstm.executeQuery();
			admin =new Admin();
			while(rs.next()){
				
				admin.setA_num(rs.getInt(1));
				admin.setA_password(rs.getInt(2));
			}
			return admin;
		} catch (SQLException e) {
			
			e.printStackTrace();
			return null;
		}
	}
	
	public List<Student> findAllStudent(){
		try{
			List<Student> list=new ArrayList<Student>();
			Connection con=BaseDao.getCon();
			PreparedStatement pstm=con.prepareStatement("select * from student");
			ResultSet rs=pstm.executeQuery();
			Student p=null;
			while(rs.next()){
				p=new Student();
			    p.setS_name(rs.getString(1));
			    p.setS_sex(rs.getString(2));
			    p.setS_numb(rs.getInt(3));
			    p.setS_phone(rs.getInt(4));
				list.add(p);
				System.out.println(p);
			}
			
			return list;
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}
	
	
	
	public List<Manage> findAllManage(){
		try{
			List<Manage> listm=new ArrayList<Manage>();
			Connection con=BaseDao.getCon();
			PreparedStatement pstm=con.prepareStatement("select * from Manage");
			ResultSet rs=pstm.executeQuery();
			Manage m=null;
			while(rs.next()){
				m=new Manage();
			    m.setM_num(rs.getInt(1));
			    m.setM_name(rs.getString(2));
			    m.setM_sex(rs.getString(3));
				
				listm.add(m);
				System.out.println(m);
			}
			
		   return listm;
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}
	
	
	public boolean deleteStudentBys_numb(int s_numb){
		try{
			Connection con=BaseDao.getCon();
			PreparedStatement pstm=con.prepareStatement("delete from student where s_numb=?");
			pstm.setInt(1, s_numb);
			int rs=pstm.executeUpdate();
			if(rs>0){
				return true;
			}else{
				return false;
			}
		}catch(Exception e){
			e.printStackTrace();
			return false;
		}
	}
	
	public boolean deleteManageBym_num(int m_num){
		try{
			Connection con=BaseDao.getCon();
			PreparedStatement pstm=con.prepareStatement("delete from manage where m_num=?");
			pstm.setInt(1, m_num);
			int rs=pstm.executeUpdate();
			System.out.println(m_num);
			
			if(rs>0){
				return true;
			}else{
				return false;
			}
		}catch(Exception e){
			e.printStackTrace();
			return false;
		}
	}
	
	public boolean udateManage(Manage manage){
		
		Connection con=BaseDao.getCon(); 
		try {
			PreparedStatement p = con.prepareStatement("update manage set m_name=?,m_sex=? where m_num=?");
	
		p.setString(1, manage.getM_name());
		p.setString(2,manage.getM_sex());
		p.setInt(3, manage.getM_num());
		int rs = p.executeUpdate();
		System.out.println(rs);
		if(rs>0){
			return true;
		}else{
			return false;
		}
		} catch (SQLException e) {
			
			e.printStackTrace();
			return false;
		}
	}
	
public boolean udateStudent(Student stu){
		
		Connection con=BaseDao.getCon(); 
		try {
			PreparedStatement p = con.prepareStatement("update student set s_name=?,s_sex=?,s_phone=? where s_numb=?");
		p.setString(1, stu.getS_name());
		p.setString(2,stu.getS_sex());
		p.setInt(3, stu.getS_phone());
		p.setInt(4, stu.getS_numb());
		int rs = p.executeUpdate();
		System.out.println(rs);
		if(rs>0){
			return true;
		}else{
			return false;
		}
		} catch (SQLException e) {
			
			e.printStackTrace();
			return false;
		}
	}
	
  


public Manage findManageByNum(int m_num){

	try {
		Connection con=BaseDao.getCon();
		PreparedStatement pstm=con.prepareStatement("select * from manage where m_num");
        pstm.setInt(1, m_num);
		ResultSet rs=pstm.executeQuery();
		Manage ma = null;
		while(rs.next()){
			ma=new Manage();
			ma.setM_num(rs.getInt(1));
			ma.setM_name(rs.getString(2));
			ma.setM_sex(rs.getString(3));
		}
		return ma;
	} catch (SQLException e) {
		
		e.printStackTrace();
		return null;
	}
}

public boolean addStudent(Student u){
	
	try {
		Connection conn=BaseDao.getCon();
		PreparedStatement pstm=conn.prepareStatement("insert into student(s_name,s_sex,s_numb,s_phone) values(?,?,?,?)");
        pstm.setString(1, u.getS_name());
        pstm.setString(2,u.getS_sex());
        pstm.setInt(3, u.getS_numb());
        pstm.setInt(4, u.getS_phone());
           
	    int rs = pstm.executeUpdate();
	    if(rs>0){
			return true;
		}else{
			return false;
		}
	} catch (SQLException e) {
		
		e.printStackTrace();
		return false;
	}
}
	
public boolean addManage(Manage m){
	
	try {
		Connection conn=BaseDao.getCon();
		PreparedStatement pstm=conn.prepareStatement("insert into manage(m_num,m_name,m_sex) values(?,?,?)");
        pstm.setInt(1,m.getM_num());
        pstm.setString(2,m.getM_name());
        pstm.setString(3,m.getM_sex());
           
	    int rs = pstm.executeUpdate();
	    if(rs>0){
			return true;
		}else{
			return false;
		}
	} catch (SQLException e) {
		
		e.printStackTrace();
		return false;
	}
}
	
}
