package com.Manages.dao.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.Manages.dao.BaseDao;
import com.Manages.entity.Users;

public class UsersDaoImpl {

	

	
	
public boolean addMUsers(Users u){
	
		try {
			Connection conn=BaseDao.getCon();
			PreparedStatement pstm=conn.prepareStatement("insert into users(user_name,user_password,iden,m_numb) values(?,?,?,?)");
	        pstm.setString(1, u.getUser_name());
	        pstm.setString(2,u.getUser_password());
	        pstm.setInt(3, u.getIden());
	        pstm.setInt(4, u.getM_numb());
	           
		    int rs = pstm.executeUpdate();
		    if(rs>0){
				return true;
			}else{
				return false;
			}
		} catch (SQLException e) {
			
			e.printStackTrace();
			return false;
		}
	}

          
    

    public boolean  findUsersByUsername(Users users){
    	
    	try {
    		Connection conn=BaseDao.getCon();
			PreparedStatement pstm=conn.prepareStatement("select * from users where user_name=? and user_password=?");
            pstm.setString(1, users.getUser_name());
    	    pstm.setString(2, users.getUser_password());

    	    ResultSet rs=pstm.executeQuery();
    	    users=new Users();
    	    while(rs.next()){
    	    	users.setUser_name(rs.getString("user_name"));
    	    	users.setUser_password(rs.getString("user_password"));

    	    	System.out.println(users);
    	    }
    	    if(users!=null){
				 return true;
			 }else{
	     		 return false;
			 }
    	} catch (SQLException e) {
			
			e.printStackTrace();
			return false;
		}
    }
    
    
}
