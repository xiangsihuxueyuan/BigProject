package com.Manages.service.Admin;

import java.util.List;

import com.Manages.dao.Admin.AdminImpl;
import com.Manages.entity.Admin;
import com.Manages.entity.Manage;
import com.Manages.entity.Student;



public class AdminService {
	AdminImpl ai=new AdminImpl();
	
	public Admin findAminByNum(Admin admin){
		Admin a=ai.findAminByNum(admin);
		return a;
	 }
	
	public Manage findManageByNum(int m_num){
		Manage man=ai.findManageByNum(m_num);
		return man;
	}

	
	public List<Student> findAllStudent(){
		List<Student> lis=ai.findAllStudent();
		return lis;
	}
	 
	public List<Manage> findAllManage(){
		List<Manage> lim=ai.findAllManage();
		return lim;
	}
	
	public boolean deleteStudentBys_numb(int s_numb){
		boolean o=ai.deleteStudentBys_numb(s_numb);
		return o;
	}
	
	public boolean deleteManageBym_num(int m_num){
		boolean b=ai.deleteManageBym_num(m_num);
		return b;
	}
	
	public boolean udateManage(Manage manage){
		boolean ud=ai.udateManage(manage);
		return ud;
	}
	
	public boolean udateStudent(Student stu){
		boolean us=ai.udateStudent(stu);
		return us;
	}
	
	public boolean addStudent(Student u){
		boolean as=ai.addStudent(u);
		return as;
	}
	
	public boolean addManage(Manage m){
		boolean am=ai.addManage(m);
		return am;
	}
}
