package com.Manages.servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.Manages.dao.Admin.AdminImpl;
import com.Manages.entity.Student;

/**
 * Servlet implementation class AdminInsertStuden
 */
@WebServlet("/AdminInsertStuden")
public class AdminInsertStuden extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AdminInsertStuden() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
		response.setContentType("text/html; charset=UTF-8");
		
		String s_name=request.getParameter("s_name");
		String s_sex = request.getParameter("s_sex");
		int s_numb = Integer.parseInt(request.getParameter("s_numb"));
		int s_phone = Integer.parseInt(request.getParameter("s_phone"));
		
		Student s = new Student(s_name,s_sex,s_numb,s_phone);
		AdminImpl an = new AdminImpl();
		boolean ins = an.addStudent(s);
		
		if(ins){
			request.getRequestDispatcher("AdminStudentList").forward(request, response);
		}else{
			response.sendRedirect("admin.jsp");
		}
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
