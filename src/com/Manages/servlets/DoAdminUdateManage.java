package com.Manages.servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.Manages.entity.Manage;
import com.Manages.service.Admin.AdminService;

/**
 * Servlet implementation class AdminUdateManage
 */
@WebServlet("/AdminUdateManage")
public class DoAdminUdateManage extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DoAdminUdateManage() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
		response.setContentType("text/html; charset=UTF-8");
		
		int m_num=Integer.parseInt(request.getParameter("m_num"));
		String  m_name=request.getParameter("m_name");
		String m_sex=request.getParameter("m_sex");
		
		Manage mg = new Manage(m_num,m_name,m_sex);
		AdminService as =new AdminService();
		List<Manage> li=as.findAllManage();
		boolean rs = as.udateManage(mg);
		
		if(rs){
			request.getRequestDispatcher("AdminManageList").forward(request, response);
		}else{
			response.sendRedirect("admin.jsp");
		}
		
	}
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
	}

}
