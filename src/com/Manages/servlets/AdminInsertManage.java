package com.Manages.servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.Manages.dao.Admin.AdminImpl;
import com.Manages.entity.Manage;

/**
 * Servlet implementation class AdminInsertManage
 */
@WebServlet("/AdminInsertManage")
public class AdminInsertManage extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AdminInsertManage() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
		response.setContentType("text/html; charset=UTF-8");
		
		
		int m_num = Integer.parseInt(request.getParameter("m_num"));
		String m_name = request.getParameter("m_name");
		String m_sex  = request.getParameter("m_sex");
		
		Manage ma = new Manage(m_num,m_name,m_sex);
		AdminImpl a = new AdminImpl();
	    boolean boo = a.addManage(ma);
	    

		if(boo){
			request.getRequestDispatcher("AdminManageList").forward(request, response);
		}else{
			response.sendRedirect("admin.jsp");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
