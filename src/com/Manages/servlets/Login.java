package com.Manages.servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.Manages.dao.User.UsersDaoImpl;
import com.Manages.entity.Users;

/**
 * Servlet implementation class Login
 */
@WebServlet("/Login")
public class Login extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Login() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
		response.setContentType("text/html; charset=UTF-8");	
		
		String user_name=request.getParameter("user_name"); 
		String user_password=request.getParameter("user_password");
	
		
		Users users=new Users(user_name,user_password);
		
		if(user_name!=null&&user_name.length()>0&&user_password!=null&&user_password.length()>0){
			
		
		UsersDaoImpl udao=new UsersDaoImpl();
	
		boolean result=udao.findUsersByUsername(users);
		
		
		
		if(result){
			request.getRequestDispatcher("N.jsp").forward(request, response);
		}else{
			response.sendRedirect("login.jsp");
		}
		
		
        
	}
	}
		
		
	

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		
		
	}
}
