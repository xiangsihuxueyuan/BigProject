package com.Manages.servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.Manages.dao.User.UsersDaoImpl;
import com.Manages.entity.Users;


/**
 * Servlet implementation class Login
 */
@WebServlet("/Register")
public class Register extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * Default constructor. 
     */
    public Register() {
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
		response.setContentType("text/html; charset=UTF-8");
		
		
		
		String user_name=request.getParameter("user_name");
		String user_password=request.getParameter("user_password");
		int numb=Integer.parseInt(request.getParameter("numb"));
		int iden=Integer.parseInt(request.getParameter("iden"));
		Users users=new Users();
		
		if(user_name!=null&&user_name.length()>0&&user_password!=null&&user_password.length()>0){
		UsersDaoImpl ud=new UsersDaoImpl();
		
		if(iden==0){
			users.setUser_name(user_name);
			users.setUser_password(user_password);
			users.setS_numb(numb);
			users.setIden(iden);
			System.out.println(users);
			boolean qq= ud.addMUsers(users);
			
		}else{
			users.setUser_name(user_name);
			users.setUser_password(user_password);
			users.setM_numb(numb);
			users.setIden(iden);
			System.out.println(users);
			boolean qq= ud.addMUsers(users);
		}
		
		request.getRequestDispatcher("login.jsp").forward(request, response);
		
	    }

}
	

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
	}
	}
