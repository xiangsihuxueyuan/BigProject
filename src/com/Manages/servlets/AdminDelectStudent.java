package com.Manages.servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.Manages.dao.Admin.AdminImpl;
import com.Manages.service.Admin.AdminService;

/**
 * Servlet implementation class AdminDelect
 */
@WebServlet("/AdminDelect")
public class AdminDelectStudent extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AdminDelectStudent() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
		response.setContentType("text/html; charset=UTF-8");
	
		String s_numb=request.getParameter("s_numb");
	
	
	AdminService as=new AdminService();
	boolean result=as.deleteStudentBys_numb(Integer.parseInt(s_numb));
	//AdminImpl ai=new AdminImpl();
	//boolean result=ai.deleteStudentBys_numb(Integer.parseInt(s_numb));
	System.out.println(s_numb);
	
	if(result){
	request.getRequestDispatcher("AdminStudentList").forward(request, response);
	}else{
		response.sendRedirect("admin.jsp");
	}
	
}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
	}

}
