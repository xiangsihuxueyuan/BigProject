<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:set var="ctx" value="${pageContext.request.contextPath}" />
<!DOCTYPE html>
<html>
<head>
    <title></title>
    <meta charset="utf-8"/>
    <link href="css/reset.css" rel="stylesheet" type="text/css">
	<link rel="stylesheet" type="text/css" href="css/search-form.css">
	<link rel="stylesheet" href="css/main.css" />
    <style type="text/css">

        body
        {
            font-size: 14px;
            font-family: Microsoft YaHei, Tahoma, Geneva, sans-serif;
        }

        #content ul
        {  margin-top: 0px;
        	padding: 0px;
             height: 0px;
            width: 1000px;
            
        }

        #content ul li
        {
  
            margin-right: 20px;
            margin-top: 40px;
            width: 225px;
            height: 180px;
            float: left;
        }

        #content ul li:last-child
        {
            margin-right: 0;
        }

        #content ul li a
        {
            position: relative;
            display: block;
            width: 100%;
            height: 100%;
            /*舞台（动画元素的父容器）perspective*/
            -webkit-perspective: 800px;
            -moz-perspective: 800px;

        }

        #content ul li a > div
        {
            position: absolute;
            left: 0;
            height: 0;
            width: 100%;
            height: 100%;
            color: #fff;
            /*动画元素transform-style*/
            -webkit-transform-style: preserve-3d;
            -moz-transform-style: preserve-3d;
            -webkit-transition: .8s ease-in-out ;
            -moz-transition:  .8s ease-in-out ;
            /*动画元素背后设置为hidden*/
            -webkit-backface-visibility: hidden;
            -moz-backface-visibility: hidden;
        }

        #content ul li a div:first-child
        {
            /*
            绕y轴旋转
            */
            -webkit-transform: rotateY(0);
            -moz-transform: rotateY(0);
            z-index: 2;
        }

        #content ul li a div:last-child
        {
            background: url("images/bg.jpg") no-repeat 0 0;
            -webkit-transform: rotateY(180deg);
            -moz-transform: rotateY(180deg);
            z-index: 1;
        }

        #content ul li a:hover div:first-child
        {
            -webkit-transform: rotateY(-180deg);
            -moz-transform: rotateY(-180deg);
        }

        #content ul li a:hover div:last-child
        {
            -webkit-transform: rotateY(0);
            -moz-transform: rotateY(0);
        }

        #content ul li a div h3
        {
            margin: 0 auto 15px;
            padding: 15px 0;
            width: 200px;
            height: 16px;
            line-height: 16px;
            font-size: 14px;
            text-align: center;
            border-bottom: 1px #fff dashed;
        }

        #content ul li a div p
        {
            padding: 0 10px;
            font-size: 12px;
            text-indent: 2em;
            line-height: 18px;
        }


	 .ul4 li{
		list-style: none;
		float: left;
		width: auto;
		height: 40px;
		font-size: 14px;
		padding: 5px 10px;
		line-height: 30px;
		-webkit-box-sizing:border-box;
		-moz-box-sizing:border-box;
		box-sizing: border-box;
		cursor: pointer;
	}
	img{
	height:180px;
	width:225px
	}
    </style>
</head>
<body>
	
<div style="height: 200px; background-color: deepskyblue;">
	
	<div style="float: left;margin: 0 auto;margin-left: 50px; margin-top: 30px;" class="cd">
	<ul class="ul4" style="height: 40px;
		clear: both;
		color: #fff;
		margin: 0;
		position: relative;
		z-index: 2;
		padding: 0 20px;
		float: left;">
			<li><a href="ToShowB">首页</a></li>
			<li><a href="ShowLendToS">借阅记录</a></li>
			<li><a href="#">个人中心</a></li>
			<li  style="display:${string};"><a href="index.jsp">图书管理</a></li>
		</ul>
		<div style="margin-top: 120px ; margin-left: 50px;">
<ul id="lb" class="ul3" >
			<li><a href="ToShowByCate?category=科幻&times=1">科幻</a></li>
			<li><a href="ToShowByCate?category=小说&times=1">小说</a></li>
			<li><a href="ToShowByCate?category=名著&times=1">名著</a></li>
		</ul>
		</div>
	</div>
	
	<div style="float: right;">
	<header class="htmleaf-header">
	</header>
	<section class="container">
	
		<form action="ToShowByName">
		<input type="hidden" value="1" name="times"/>
            <div class="search-wrapper">
                <div class="input-holder">
                    <input type="text"  name="b_name" class="search-input" placeholder="请输入书名" />
                    <button class="search-icon" onclick="searchToggle(this, event);"><span></span></button>
                </div>
                <span class="close" onclick="searchToggle(this, event);"></span>
                <div class="result-container">
                </div>
            </div>
        </form>
	</section>
	</div>
</div>
	<div style="height:1000 ; width: 1000px; margin: 0 auto;">
	
	
<div id="content">
    <ul>
    <c:forEach items="${book}" var="b">
        <li>
            <a href="#" >
                <div><img alt="" src="images/${b.listimg}"/></div>
                <div>
                    <h3>编号：${b.b_numb} </h3>
                    <p>类别：${b.category}</p>
                     <p>价格：${b.price}</p>
                      <p>是否在馆：${b.state } </p>
                    <p>简介：${b.introdution}</p>
                </div>
            </a>
              <p style="float: left; width: 225px; text-align: center;">${b.b_name}</p>
        </li>
        </c:forEach>
    </ul>
</div>

</div>

<div style="height: 730px;"></div>


<div style="height: 50px; width: 100%;">
	<div style="float: right; margin-right: 100px;"class="yw">
		<a href="ToShowB?pageNow=1">首页</a>
	<a href="ToShowB?pageNow=${page.prePageNum}">上一页</a>
	<a href="ToShowB?pageNow=${page.nextPageNum}">下一页</a>
	<a href="ToShowB?pageNow=${page.zy}">尾页</a>
	<span>当前页数${page.pageNum}</span>
	<span>总页数${page.zy}</span>
	</div>
</div>


<script src="http://www.jq22.com/jquery/1.11.1/jquery.min.js"></script>
	<script>window.jQuery || document.write('<script src="js/jquery-1.11.0.min.js"><\/script>')</script>
	<script type="text/javascript">
        function searchToggle(obj, evt){
            var container = $(obj).closest('.search-wrapper');

            if(!container.hasClass('active')){
                  container.addClass('active');
                  evt.preventDefault();
            }
            else if(container.hasClass('active') && $(obj).closest('.input-holder').length == 0){
                  container.removeClass('active');
                  // clear input
                  container.find('.search-input').val('');
                  // clear and hide result container when we press close
                  container.find('.result-container').fadeOut(100, function(){$(this).empty();});
            }
        }

        function submitFn(obj, evt){
            value = $(obj).find('.search-input').val().trim();

            _html = "Yup yup! Your search text sounds like this: ";
            if(!value.length){
                _html = "Yup yup! Add some text friend :D";
            }
            else{
                _html += "<b>" + value + "</b>";
            }

            $(obj).find('.result-container').html('<span>' + _html + '</span>');
            $(obj).find('.result-container').fadeIn(100);

            evt.preventDefault();
        }
    </script>
</body>


<script src="http://www.jq22.com/jquery/jquery-1.10.2.js"></script>
<script src="js/jquery.easing.js"></script>
<script src="js/toast.js"></script>
<script src="js/moveline.js"></script>
<script>
	$('.ul4').moveline({color:'#99D1F6',position:'inner',height:40});
	$('.ul3').moveline({color:'#99D1F6',position:'inner'});
</script>
</html>