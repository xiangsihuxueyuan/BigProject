<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:set var="ctx" value="${pageContext.request.contextPath}" />
<!DOCTYPE html>
<html lang="en" class="no-js">

    <head>

        <meta charset="utf-8">
        <title>Fullscreen Login</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <!-- CSS -->
        <link rel="stylesheet" href="assets/css/reset.css">
        <link rel="stylesheet" href="assets/css/supersized.css">
        <link rel="stylesheet" href="assets/css/style.css">


    </head>

    <body>

        <div class="page-container">
            <h1>Login</h1>
            <form action="ToRegister">
                <input type="text" name="user_name" class="username" placeholder="用户名">
                <input type="password" name="user_password" class="userpassword" placeholder="密码">
                <input type="text" name="numb" class="username" placeholder="学号/工号">
                <select name="iden">
                	<option value="0">
                		学生
                	</option>
                	<option value="1">
                		管理员
                	</option>
                </select>
                 <button type="submit">注册</button>
            </form>
         <div class="connect">
               	<a href="login.jsp" >返回登陆</a>
            </div>
        </div>
        <!-- Javascript -->
        <script src="assets/js/jquery-1.8.2.min.js"></script>
        <script src="assets/js/supersized.3.2.7.min.js"></script>
        <script src="assets/js/supersized-init.js"></script>
        <script src="assets/js/scripts.js"></script>
    </body>

</html>

