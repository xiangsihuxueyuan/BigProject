<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:set var="ctx" value="${pageContext.request.contextPath}" />
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
	<table>
	<c:forEach items="${list}" var="l">
		<tr>
			<td>书名:</td>
			<td>借出日期:${l.lend_data}</td>
			<td>最迟归还:${l.late_data}</td>
			<td>归还日期${l.return_data}</td>
		</tr>
		</c:forEach>
	</table>
</body>
</html>