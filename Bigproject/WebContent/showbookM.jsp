<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:set var="ctx" value="${pageContext.request.contextPath}" />
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
<table>
		<tr>
			<td>编号</td>
			<td>书名</td>
			<td>加入时间</td>
			<td>类行</td>
			<td>作者</td>
			<td>出版商</td>
			<td>价格</td>
			<td>简介</td>
			<td>是否在馆</td>
		</tr>
		<c:forEach items="${book}" var="b">
		<tr>
			<td>${b.b_numb }</td>
			<td>${b.b_name }</td>
			<td>${b.input_data }</td>
			<td>${b.category }</td>
			<td>${b.author }</td>
			<td>${b.press }</td>
			<td>${b.price }</td>
			<td>${b.introdution}</td>
			<td>${b.state }</td>
		</tr>
		</c:forEach>
	</table>
	
	<div style="height: 50px; width: 100%;">
	<div style="float: right; margin-right: 100px;"class="yw">
		<a href="ShowBookToM?pageNow=1">首页</a>
	<a href="ShowBookToM?pageNow=${page.prePageNum}">上一页</a>
	<a href="ShowBookToM?pageNow=${page.nextPageNum}">下一页</a>
	<a href="ShowBookToM?pageNow=${page.zy}">尾页</a>
	<span>当前页数${page.pageNum}</span>
	<span>总页数${page.zy}</span>
	</div>
</div>
</body>
</html>