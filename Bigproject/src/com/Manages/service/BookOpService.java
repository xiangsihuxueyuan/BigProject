package com.Manages.service;

import com.Manages.dao.BookOperation;
import com.Manages.entity.Book;
import com.Manages.entity.Lend;

public class BookOpService{

	private BookOperation bo=new BookOperation();
	
	//添加书籍
	public int AddBook(Book b) {
		int count=bo.addBook(b);
		return count;
	}
	
	
	//删除书籍
	public int DeleteBookByNumb(int b_num) {
		int count=bo.deleteBookByNumb(b_num);
		return count;
	}
	
	//寻找书籍
	public boolean selectBookAdd(int b_numb) {
		boolean bs = bo.selectBookAdd(b_numb);
		return bs;
	}
	
	//借出书籍
	public boolean addLend(Lend l,int state) {
		int i = bo.addLend(l,state);
		boolean b = false;
		if(i== 1 ) {
			b =true;
		}
		return b;
	}
	
	
	//补全书籍信息
	public Book selectBookbq(int b_num) {
		Book b=  bo.selectBookbq(b_num);
		return b;
	}
}
