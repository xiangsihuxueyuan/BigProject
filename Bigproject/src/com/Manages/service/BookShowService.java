package com.Manages.service;

import java.util.List;

import com.Manages.dao.BookShow;
import com.Manages.entity.Book;

public class BookShowService {

	private BookShow bs=new BookShow();
	
	//按书编号查找
	public Book findBookByNumb(int b_numb) {
		Book b=bs.findBookByNumb(b_numb);
		return b;
	}
	
	//按名查找
	public List<Book> findBookByName(String b_name,int pageNow,int pageSize){
		List<Book> list=bs.findBookByName(b_name, pageNow, pageSize);
		return list;
	}
	
	//按类别查找
	public List<Book> findBookByCategory(String category,int pageNow,int pageSize){
		List<Book> list=bs.findBookByCategory(category, pageNow, pageSize);
		return list;
	}
	
	//查找所有
	public List<Book> findBookAll(int pageNow,int pageSize){
		List<Book> list =bs.findBookAll(pageNow, pageSize);
		return list;
	}
	
	//查询说有数据
	public int findBookCount() {
		int cont = bs.findBookCount();
		return cont;
	}
	
	public int findBookCountByCata(String category) {
		int cont = bs.findBookCountByCata(category);
		return cont;
	}
	
	public int findBookCountByName(String b_name) {
		int cont =bs.findBookCountByName(b_name);
		return cont;
	}
}
