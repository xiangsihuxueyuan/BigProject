package com.Manages.service;

import java.util.List;

import com.Manages.dao.UserDao;
import com.Manages.entity.Lend;
import com.Manages.entity.User;

public class UserOpService {

	private UserDao ud=new UserDao(); 
	
	/**
	 * 添加用户
	 * @param u
	 * @return
	 */
	public int addUser(User u) {
		int i=ud.addUser(u);
		return i;
	}
	
	
	/**
	 * 查看用户是否存在，登陆用
	 * @param u
	 * @return
	 */
	public boolean selectUser(User u) {
		boolean b=ud.selectUser(u);
		return b;
	}
	
	/**
	 * 查看用户是否存在，注册用
	 * @param u
	 * @return
	 */
	public boolean selectUserByNumb(User u) {
		boolean b=ud.selectUserByNumb(u);
		return b;
	}
	
	/**
	 * 补全用户信息
	 * @param u
	 * @return
	 */
	public User selectUserAdd(User u) {
		u=ud.selectUserAdd(u);
		return u;
	}
	
	/**
	 * 删除用户
	 * @param b_num
	 * @param iden
	 * @return
	 */
	public int deleteUserByNumb(int b_num,int iden) {
		int i = ud.deleteUserByNumb(b_num, iden);
		return i;
	}
	
	
	/**
	 * 
	 * @param pageNow
	 * @param pageSize
	 * @return
	 */
	
	public List<User> findUserAll(int pageNow,int pageSize){
		List<User> list =ud.findUserAll(pageNow, pageSize);
		return list;
	}
	
	
	
	/**
	 * 查找用户借阅记录（学生用）
	 * @param s_numb
	 * @return
	 */
	public List<Lend> findLendbyu_name(int s_numb){
		List<Lend> list = ud.findLendbyu_name(s_numb);
		return list;
	}
	
	
	/**
	 *查询所有借阅记录（管理员用）
	 * @return
	 */
	public List<Lend> findLend(){
		List<Lend> list = ud.findLend();
		return list;
	}
	
	/**
	 * 通过学号查询是否存在
	 * @param s_numb
	 * @return
	 */
	public boolean selectUserByNumb(int s_numb) {
		boolean b = ud.selectUserByNumb(s_numb);
		return b;
	}
}
