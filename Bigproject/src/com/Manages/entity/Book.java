package com.Manages.entity;

public class Book {

	/**
	 * 编号
	 * 书名
	 * 加入日期
	 * 类型
	 * 作者
	 * 出版商
	 * 价格
	 * 简介
	 * 状态
	 * 图片
	 */
	private int b_numb;
	private String b_name;
	private String input_data;
	private String category;
	private String author;
	private String press;
	private int price;
	private String introdution;
	private int state;
	private String listimg;
	
	public Book() {
		super();
	}
	
	public Book(int b_numb, String b_name, String input_data, String category, String author, String press, int price,
			String introdution, int state, String listimg) {
		super();
		this.b_numb = b_numb;
		this.b_name = b_name;
		this.input_data = input_data;
		this.category = category;
		this.author = author;
		this.press = press;
		this.price = price;
		this.introdution = introdution;
		this.state = state;
		this.listimg = listimg;
	}
	public int getB_numb() {
		return b_numb;
	}
	public void setB_numb(int b_numb) {
		this.b_numb = b_numb;
	}
	public String getB_name() {
		return b_name;
	}
	public void setB_name(String b_name) {
		this.b_name = b_name;
	}
	public String getInput_data() {
		return input_data;
	}
	public void setInput_data(String input_data) {
		this.input_data = input_data;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public String getPress() {
		return press;
	}
	public void setPress(String press) {
		this.press = press;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	public String getIntrodution() {
		return introdution;
	}
	public void setIntrodution(String introdution) {
		this.introdution = introdution;
	}
	public int getState() {
		return state;
	}
	public void setState(int state) {
		this.state = state;
	}
	public String getListimg() {
		return listimg;
	}
	public void setListimg(String listimg) {
		this.listimg = listimg;
	}

	@Override
	public String toString() {
		return "Book [b_numb=" + b_numb + ", b_name=" + b_name + ", input_data=" + input_data + ", category=" + category
				+ ", author=" + author + ", press=" + press + ", price=" + price + ", introdution=" + introdution
				+ ", state=" + state + ", listimg=" + listimg + "]";
	}
	
	
	
	
}
