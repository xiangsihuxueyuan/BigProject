package com.Manages.entity;

public class Lend {

	private int s_numb;
	private int b_numb;
	private String  lend_data;
	private String  late_data;
	private String  return_data;
	
	
	
	
	public int getS_numb() {
		return s_numb;
	}
	public void setS_numb(int s_numb) {
		this.s_numb = s_numb;
	}
	public int getB_numb() {
		return b_numb;
	}
	public void setB_numb(int b_numb) {
		this.b_numb = b_numb;
	}
	public String getLend_data() {
		return lend_data;
	}
	public void setLend_data(String lend_data) {
		this.lend_data = lend_data;
	}
	public String getLate_data() {
		return late_data;
	}
	public void setLate_data(String late_data) {
		this.late_data = late_data;
	}
	public String getReturn_data() {
		return return_data;
	}
	public void setReturn_data(String return_data) {
		this.return_data = return_data;
	}
	@Override
	public String toString() {
		return "Lend [s_numb=" + s_numb + ", b_numb=" + b_numb + ", lend_data=" + lend_data + ", late_data=" + late_data
				+ ", return_data=" + return_data + "]";
	}
	
	
	
	
	
}
