package com.Manages.entity;


public class Page<T> {

	private int pageNum; //当前页数
	private int pageSize;//页数显示多少
	private int prePageNum; //上一页
	private int nextPageNum; //下一页
	private int my; //末页
	private int zy; //总页数
	
	
	public int getPageNum() {
		return pageNum;
	}
	public void setPageNum(int pageNum) {
		this.pageNum = pageNum;
		
		if(this.pageNum>1) {
		this.prePageNum=this.pageNum-1;
		}else if(pageNum==1){
			this.prePageNum=1;
		}
		
		
		
		if(this.zy>this.pageNum) {
			this.nextPageNum=this.pageNum+1;
		}else if(this.zy==this.pageNum) {
			this.nextPageNum=this.pageNum;
		}
	}
	
	public int getPageSize() {
		return pageSize;
	}
	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}
	public int getPrePageNum() {
		return prePageNum;
	}
	public void setPrePageNum(int prePageNum) {
		this.prePageNum = prePageNum;
	}
	public int getNextPageNum() {
		return nextPageNum;
	}
	public void setNextPageNum(int nextPageNum) {
		this.nextPageNum = nextPageNum;
	}
	public int getMy() {
		return my;
	}
	public void setMy(int my) {
		this.my = my;
	}
	public int getZy() {
		return zy;
	}
	
	public void setZy(int zsj) {
		if(zsj %12 ==0 && zsj/12 !=0) {
			this.zy=zsj/12;
		}else if(zsj%12 != 0 && zsj/12 == 0) {
			this.zy=1;
		}else {
			this.zy=(zsj/12)+1;
		}
	}
}
