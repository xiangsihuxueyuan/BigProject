package com.Manages.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.Manages.entity.Book;
import com.Manages.entity.Lend;


public class BookOperation {

	//管理员特有 
	
	
	//添加操作  insert into book values(7,'白夜行','2018-12-12','小说','东归野古','jp',45,'一本小说',1,'asjldkj')
	//删除操作 delete from book where b_numb=7
	//修改操作 update book set b_name='hzm' where b_numb=5

	
	//添加书籍
	public int addBook(Book b){
		int count = 0;
		PreparedStatement ps;
	 try {
		 Connection con=BaseDao.getCon();
		String sql = " insert into book values(?,?,?,?,?,?,?,?,?,?)";
		  ps = con.prepareStatement(sql);
		  ps.setInt(1, b.getB_numb());
		  ps.setString(2, b.getB_name());
		  ps.setString(3, b.getInput_data());
		  ps.setString(4, b.getCategory());
		  ps.setString(5, b.getAuthor());
		  ps.setString(6, b.getPress());
		  ps.setInt(7, b.getPrice());
		  ps.setString(8, b.getIntrodution());
		  ps.setInt(9, b.getState());
		  ps.setString(10, b.getListimg());
		 count = ps.executeUpdate();
	} catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	return count;
}
	
	
	//删除书籍
	
	public int deleteBookByNumb(int b_num){
		 int i =-1;
		 PreparedStatement ps;
		try {
			Connection con=BaseDao.getCon();
			ps=con.prepareStatement("delete from book where b_numb=?");
			ps.setInt(1, b_num);
			i= ps.executeUpdate();
		} catch (SQLException e) {	
			e.printStackTrace();
		}	
			return i;
		}
	
	
	//判断书籍编号是否存在
	public boolean selectBookAdd(int b_num) {
		int count = 0;
		try{
			Connection con=BaseDao.getCon();
			PreparedStatement ps=con.prepareStatement("select count(*) from book where b_numb=?");
		    ps.setInt(1,b_num);
			ResultSet rs=ps.executeQuery();
			while(rs.next()){
			count = rs.getInt(1);	
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		if(count ==1) {
			return true;
		}else {
			return false;
		}
	}
	
	//补全书籍信息，暂时补全状态
	public Book selectBookbq(int b_num) {
		int count = 0;
		Book  b =new Book();
		try{
			Connection con=BaseDao.getCon();
			PreparedStatement ps=con.prepareStatement("select * from book where b_numb=?");
		    ps.setInt(1,b_num);
			ResultSet rs=ps.executeQuery();
			while(rs.next()){
			b.setState(rs.getInt("state"));	
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return b;
	}
	
	//借书
	public int addLend(Lend l,int state){
		int count = 0;
		PreparedStatement ps;
	 try {
		 Connection con=BaseDao.getCon();
		String sql = " insert into lend values(?,?,?,?,?)";
		  ps = con.prepareStatement(sql);
		  ps.setInt(1,l.getS_numb());
		  ps.setInt(2,l.getB_numb());
		  ps.setString(3,l.getLend_data());
		  ps.setString(4, l.getLate_data());
		  ps.setString(5,l.getReturn_data());
		  count = ps.executeUpdate();
	} catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	 int up=this.upDateBookByL(l.getB_numb(),state);
	 if(up==1) {
		 System.out.println("书籍修改成功");
	 }else {
		 System.out.println("书籍修改失败");
	 }
	return count;
}
	
	//改便书籍信息（借/还时）
	public int upDateBookByL(int b_umb ,int state) {
		int count = 0;
		PreparedStatement ps;
		try {
			 Connection con=BaseDao.getCon();
			String sql = " update  book set state=? where b_numb=?";
			  ps = con.prepareStatement(sql);
			  ps.setInt(1,state);
			  ps.setInt(2,b_umb);
			 count = ps.executeUpdate();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return count;
	}
	
	
}
