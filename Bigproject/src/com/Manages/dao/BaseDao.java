package com.Manages.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.text.SimpleDateFormat;
import java.util.Date;

public class BaseDao {
	
	static{
		try{
			Class.forName("com.mysql.jdbc.Driver");
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public static Connection getCon(){
		try{
			Connection con=DriverManager.getConnection(
					"jdbc:mysql://127.0.0.1:3306/book_manage?useUnicode=true&serverTimezone=GMT&characterEncoding=UTF-8","root","root");
			return con;
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}

	
	public static String getdate(){
		Date d = new Date();
		SimpleDateFormat b = new SimpleDateFormat("yyyy-MM-dd"); 
		String data = b.format(d);
		return data;
	}
	
	
	public static String latedata() {
		Date d = new Date();
		SimpleDateFormat b = new SimpleDateFormat("yyyy-MM-dd");
		String data = b.format(d);
		String[] parts = data.split("-");
		int yue=Integer.parseInt(parts[1]);
		if(yue <12) {
			yue+=1;
			parts[1]=yue+"";
		}else {
			int nian=Integer.parseInt(parts[0]);
			nian+=1;
			parts[0]=nian+"";
			parts[1]="1";
		}
		
		StringBuilder sb = new StringBuilder();
	    String str;
	    for(int i=0;i<parts.length;i++) {
	    	if(i!=parts.length-1) {
	    		sb.append(parts[i]+"-");
	    	}else {
	    		sb.append(parts[i]);
	    	}
	    }
		return sb.toString();
	}
}
