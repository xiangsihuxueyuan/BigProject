package com.Manages.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.Manages.entity.Book;
import com.Manages.entity.Lend;
import com.Manages.entity.User;

public class UserDao {

	//添加用户
	public int addUser(User u){
		int count = 0;
		PreparedStatement ps;
	 try {
		 Connection con=BaseDao.getCon();
		String sql = " insert into users values(?,?,?,?,?)";
		  ps = con.prepareStatement(sql);
		  ps.setString(1, u.getUser_name());
		  ps.setString(2, u.getUser_password());
		  ps.setInt(3, u.getIden());
		  ps.setInt(4, u.getS_numb());
		  ps.setInt(5, u.getM_umb());
		  System.out.println(u);
		 count = ps.executeUpdate();
	} catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	 
	return count;
}
	
	//查看是否存在用户名及密码(登陆用)
	public boolean selectUser(User u) {
		int count = 0;
		boolean b=false;
		try{
			Connection con=BaseDao.getCon();
			PreparedStatement ps=con.prepareStatement("select count(*) from users where user_name=? and user_password=?");
		    ps.setString(1,u.getUser_name());
		    ps.setString(2, u.getUser_password());
			ResultSet rs=ps.executeQuery();
			while(rs.next()){
				count=rs.getInt(1);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		
		if(count == 1) {
			b=true;
		}
		return b;
	}
	
	
	//查看用户学号/工号 是否注册过(注册用)
	public boolean selectUserByNumb(User u) {
		int count = 0;
		boolean b=false;
		String iden="s_numb";
		if(u.getIden()==1) {
			iden="m_numb";
		}
		System.out.println(u);
		System.out.println(iden);
		String sql="select count(*) from users where "+iden+"=?";
		try{
			Connection con=BaseDao.getCon();
			PreparedStatement ps=con.prepareStatement(sql);
			if(u.getIden()==1) {
				ps.setInt(1,u.getM_umb());
			}else {
				ps.setInt(1, u.getS_numb());
			}
		    
			ResultSet rs=ps.executeQuery();
		
			while(rs.next()){
				count=rs.getInt(1);	
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		System.out.println("用户存在否count"+count);
		if(count == 1) {
			b=true;
		}
		
		boolean qw=this.selectUser(u);
		
		
		System.out.println("用户密码存在否？"+qw);
		System.out.println("用户numb？"+b);
		if(b || qw) {
			return true;
		}else {
			return false;
		}
	}
	
	//补全用户信息
	public User selectUserAdd(User u) {
		try{
			Connection con=BaseDao.getCon();
			PreparedStatement ps=con.prepareStatement("select * from users where user_name=? and user_password=?");
		    ps.setString(1,u.getUser_name());
		    ps.setString(2, u.getUser_password());
			ResultSet rs=ps.executeQuery();
			while(rs.next()){
				u.setIden(rs.getInt("iden"));
				u.setS_numb(rs.getInt("s_numb"));
				u.setM_umb(rs.getInt("m_numb"));
			}
		}catch(Exception e){
			e.printStackTrace();
		}

		return u;
	}
	
	
	//删除用户
	public int deleteUserByNumb(int b_num,int iden){
		 int i =-1;
			String sqlIden="s_numb";
			if(iden ==1) {
				sqlIden="m_numb";
			}
			String sql="delete from users where"+sqlIden+"=?";
		 PreparedStatement ps;
		try {
			Connection con=BaseDao.getCon();
			ps=con.prepareStatement(sql);
			ps.setInt(1, b_num);
			i= ps.executeUpdate();
		} catch (SQLException e) {	
			e.printStackTrace();
		}	
			return i;
		}
	
	
	
	//查找用户
	public List<User> findUserAll(int pageNow,int pageSize){
		Connection con=BaseDao.getCon();
		PreparedStatement ps;
		User u =null;
		int data =(pageNow-1)*pageSize;
		try {
			List<User> list=new ArrayList<User>();
			ps = con.prepareStatement("select * from users where iden = 0 limit ?,?");
			ps.setInt(1, data);
			ps.setInt(2, pageSize);
			ResultSet re=ps.executeQuery();
			while(re.next()) {
				u=new User();
                u.setIden(re.getInt("iden"));
                u.setM_umb(re.getInt("m_numb"));
                u.setS_numb(re.getInt("s_numb"));
                u.setUser_name(re.getString("user_name"));
                u.setUser_password(re.getString("user_password"));
			    System.out.println(u);
			    list.add(u);
			}
			return list;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}		
}
	
	
	
	
	//学生查询借阅记录
	public List<Lend> findLendbyu_name(int s_numb){
		Connection con=BaseDao.getCon();
		PreparedStatement ps;
		Lend l =null;
		try {
			List<Lend> list=new ArrayList<Lend>();
			ps = con.prepareStatement("SELECT * FROM lend where s_numb = ?");
			ps.setInt(1, s_numb);
			ResultSet re=ps.executeQuery();
			while(re.next()) {
				l = new Lend();
			    l.setB_numb(re.getInt("b_numb"));
			    l.setS_numb(s_numb);
			    l.setLate_data(re.getString("late_data"));
			    l.setLend_data(re.getString("lend_data"));
			    l.setReturn_data(re.getString("return_data"));
			    System.out.println(l);
			    list.add(l);
			}
			return list;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			
		}
		return null;
}
	
	
	//管理员查寻借阅记录
	public List<Lend> findLend(){
		Connection con=BaseDao.getCon();
		PreparedStatement ps;
		Lend l =null;
		try {
			List<Lend> list=new ArrayList<Lend>();
			ps = con.prepareStatement("SELECT * FROM lend");
		
			ResultSet re=ps.executeQuery();
			while(re.next()) {
				l = new Lend();
			    l.setB_numb(re.getInt("b_numb"));
			    l.setS_numb(re.getInt("s_numb"));
			    l.setLate_data(re.getString("late_data"));
			    l.setLend_data(re.getString("lend_data"));
			    l.setReturn_data(re.getString("return_data"));
			    System.out.println(l);
			    list.add(l);
			}
			return list;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
}
	
	
	
	
	
	
	//通过学号查找用户是否存在
	public boolean selectUserByNumb(int s_numb) {
		int count = 0;
		boolean b=false;
		String sql="select count(*) from users where iden=0  and s_numb=?";
		try{
			Connection con=BaseDao.getCon();
			PreparedStatement ps=con.prepareStatement(sql);
		    ps.setInt(1, s_numb);
			ResultSet rs=ps.executeQuery();
			while(rs.next()){
				count=rs.getInt(1);	
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		System.out.println("用户存在否count"+count);
		if(count == 1) {
			b=true;
		}
		return b;
	}
	
	
	//查询书籍记录是否存在
//	public boolean findLendBySM(int s_numb,int b_numb){
//		Connection con=BaseDao.getCon();
//		PreparedStatement ps;
//		boolean boo=false;
//		Lend l =new Lend();
//		try {
//			ps = con.prepareStatement("SELECT * FROM lend where s_numb = ? and b_numb=?");
//			ps.setInt(1, s_numb);
//			ps.setInt(2, b_numb);
//			ResultSet re=ps.executeQuery();
//			while(re.next()) {
//			    l.setReturn_data(re.getString("return_data"));
//			    boo = true;
//			}
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();	
//		}
//		
//}
	
}
