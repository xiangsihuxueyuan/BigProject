package com.Manages.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import com.Manages.entity.Book;


public class BookShow {

	
	//按书号查询
	public Book findBookByNumb(int b_numb) {
		Connection con =BaseDao.getCon();
		PreparedStatement ps;
		try {
			ps = con.prepareStatement("select * from book where b_numb=?");
			ps.setInt(1, b_numb);
			ResultSet re = ps.executeQuery();
			Book b = null;
			while(re.next()) {
				b=new Book();
				b.setAuthor(re.getString("author"));
				b.setB_name(re.getString("b_name"));
				b.setB_numb(b_numb);
				b.setCategory(re.getString("category"));
			    b.setInput_data(re.getString("input_data"));
			    b.setIntrodution(re.getString("introdution"));
			    b.setListimg(re.getString("listimg"));
			    b.setPress(re.getString("press"));
			    b.setPrice(re.getInt("price"));
			    b.setState(re.getInt("state"));
			}
			return b;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
				return null;
	}
	
	
	
	//按书名查找 模糊查找
	public List<Book> findBookByName(String b_name,int pageNow,int pageSize){
		Connection con=BaseDao.getCon();
		PreparedStatement ps;
		Book b =null;
		int data =(pageNow-1)*pageSize;
		try {
			List<Book> list=new ArrayList<Book>();
			ps = con.prepareStatement("select * from book where b_name like ? limit ?,?");
			ps.setString(1,"%"+b_name+"%");
			ps.setInt(2, data);
			ps.setInt(3, pageSize);
			ResultSet re=ps.executeQuery();
			while(re.next()) {
				b=new Book();
				b.setAuthor(re.getString("author"));
				b.setB_name(re.getString("b_name"));
				b.setB_numb(re.getInt("b_numb"));
				b.setCategory(re.getString("category"));
			    b.setInput_data(re.getString("input_data"));
			    b.setIntrodution(re.getString("introdution"));
			    b.setListimg(re.getString("listimg"));
			    b.setPress(re.getString("press"));
			    b.setPrice(re.getInt("price"));
			    b.setState(re.getInt("state"));
			    list.add(b);
			}
			return list;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
		
	
	//按类别查找，并显示
		public List<Book> findBookByCategory(String category,int pageNow,int pageSize){
			Connection con=BaseDao.getCon();
			PreparedStatement ps;
			Book b =null;
			int data =(pageNow-1)*pageSize;
			System.out.println("pageNow="+data);
			System.out.println(pageSize);
			System.out.println(category);
			try {
				List<Book> list=new ArrayList<Book>();
				ps = con.prepareStatement("select * from book where category=? limit ?,?");
				ps.setString(1, category);
				ps.setInt(2, data);
				ps.setInt(3, pageSize);
				ResultSet re=ps.executeQuery();
				while(re.next()) {
					b=new Book();
					b.setAuthor(re.getString("author"));
					b.setB_name(re.getString("b_name"));
					b.setB_numb(re.getInt("b_numb"));
					b.setCategory(re.getString("category"));
				    b.setInput_data(re.getString("input_data"));
				    b.setIntrodution(re.getString("introdution"));
				    b.setListimg(re.getString("listimg"));
				    b.setPress(re.getString("press"));
				    b.setPrice(re.getInt("price"));
				    b.setState(re.getInt("state"));
				    System.out.println(b);
				    list.add(b);
				}
				return list;
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return null;
			}
		}	
			
			
		//	查找所有,并显示当前界面数据
			public List<Book> findBookAll(int pageNow,int pageSize){
				Connection con=BaseDao.getCon();
				PreparedStatement ps;
				Book b =null;
				int data =(pageNow-1)*pageSize;
				try {
					List<Book> list=new ArrayList<Book>();
					ps = con.prepareStatement("select * from book limit ?,?");
					ps.setInt(1, data);
					ps.setInt(2, pageSize);
					ResultSet re=ps.executeQuery();
					while(re.next()) {
						b=new Book();
						b.setAuthor(re.getString("author"));
						b.setB_name(re.getString("b_name"));
						b.setB_numb(re.getInt("b_numb"));
						b.setCategory(re.getString("category"));
					    b.setInput_data(re.getString("input_data"));
					    b.setIntrodution(re.getString("introdution"));
					    b.setListimg(re.getString("listimg"));
					    b.setPress(re.getString("press"));
					    b.setPrice(re.getInt("price"));
					    b.setState(re.getInt("state"));
					    System.out.println(b);
					    list.add(b);
					}
					return list;
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					return null;
				}		
	}
			
			
			
			//查询所有数据 select * from book ？=？
			public int findBookCount(){
				try{
					Connection con=BaseDao.getCon();
					PreparedStatement pstm=con.prepareStatement("select count(*) from book");
					ResultSet rs=pstm.executeQuery();
					int count=0;
					while(rs.next()){
						count=rs.getInt(1);
					}
//					System.out.println(count);
					return count;
					
				}catch(Exception e){
					e.printStackTrace();
					return 0;
				}
			}
			
			
			
			public int findBookCountByCata(String category){
				try{
					Connection con=BaseDao.getCon();
					PreparedStatement pstm=con.prepareStatement("select count(*) from book where category =?");
					pstm.setString(1, category);
					ResultSet rs=pstm.executeQuery();
					int count=0;
					while(rs.next()){
						count=rs.getInt(1);
					}
					return count;
				}catch(Exception e){
					e.printStackTrace();
					return 0;
				}
			}
			
			
			public int findBookCountByName(String b_name){
				try{
					Connection con=BaseDao.getCon();
					PreparedStatement pstm=con.prepareStatement("select count(*) from book where b_name like ?");
					pstm.setString(1, "%"+b_name+"%");
					ResultSet rs=pstm.executeQuery();
					int count=0;
					while(rs.next()){
						count=rs.getInt(1);
					}
					return count;
				}catch(Exception e){
					e.printStackTrace();
					return 0;
				}
			}
			
}
