package com.Manages.servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.Manages.entity.Book;
import com.Manages.entity.Lend;
import com.Manages.entity.Page;
import com.Manages.entity.User;
import com.Manages.service.BookShowService;

/**
 * Servlet implementation class ToShowB
 */
@WebServlet("/ToShowB")
public class ToShowB extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ToShowB() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
//		response.getWriter().append("Served at: ").append(request.getContextPath());
		String string = "none";
		String pageNow = request.getParameter("pageNow");
		int nowPage=0;
		System.out.println(pageNow);
		if(pageNow==null || pageNow.equals("")){
			nowPage=1;
			System.out.println("for"+nowPage);
		}else{ 
			nowPage=Integer.parseInt(pageNow);
		}
		
		BookShowService bs=new BookShowService();
		Page p = new Page();
		//��ҳ��
		p.setZy(bs.findBookCount());
		p.setPageNum(nowPage);
		
		HttpSession session = request.getSession();
		 User u = (User)session.getAttribute("user");
		 System.out.println(u);
		 if(u.getIden()==1) {
			 string="block";
			 request.setAttribute("string", string);
		 }else {
			 request.setAttribute("string", string);
		 }
		 
		List<Book> list =bs.findBookAll(nowPage, 12);
		request.setAttribute("page", p);
		request.setAttribute("book", list);
		request.getRequestDispatcher("seleb.jsp").forward(request, response);
	
	
	   
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
