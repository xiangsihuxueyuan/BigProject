package com.Manages.servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.Manages.entity.Book;
import com.Manages.entity.Page;
import com.Manages.service.BookShowService;

/**
 * Servlet implementation class ToShowByName
 */
@WebServlet("/ToShowByName")
public class ToShowByName extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ToShowByName() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
//		response.getWriter().append("Served at: ").append(request.getContextPath());
		String b_name =request.getParameter("b_name");
		String times=request.getParameter("times");
		int nowPage=1;
		System.out.println(times);
		if(times.equals("1")){
      	  nowPage=1;
       }else{
      	  nowPage=Integer.parseInt(request.getParameter("pageNow"));
       }
         BookShowService bs=new BookShowService();
         Page p = new Page();
 		//��ҳ��
 		p.setZy(bs.findBookCountByName(b_name));
 		p.setPageNum(nowPage);
		
 		List<Book> list = bs.findBookByName(b_name, nowPage, 12);
 		request.setAttribute("b_name", b_name);
 		request.setAttribute("page", p);
		request.setAttribute("book", list);
		request.getRequestDispatcher("selectbookbyname.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
