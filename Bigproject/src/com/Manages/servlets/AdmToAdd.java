package com.Manages.servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.Manages.entity.User;
import com.Manages.service.UserOpService;

/**
 * Servlet implementation class AdmToAdd
 */
@WebServlet("/AdmToAdd")
public class AdmToAdd extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AdmToAdd() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
//		response.getWriter().append("Served at: ").append(request.getContextPath());
		String user_name=request.getParameter("user_name");
		String user_password=request.getParameter("user_password");
		int numb=Integer.parseInt(request.getParameter("numb"));
		int  iden=Integer.parseInt(request.getParameter("iden"));
		System.out.println("����"+iden);
	
		
		
		User u = new User();
	    if(iden==1) {
			u.setM_umb(numb);
		}else {
			u.setS_numb(numb);
		}
		u.setUser_name(user_name);
		u.setUser_password(user_password);
		u.setIden(iden);
		System.out.println(u);
		UserOpService uo = new UserOpService();
		boolean b = uo.selectUserByNumb(u);
		if(b){
			request.getRequestDispatcher("userlistadd.jsp").forward(request, response);
		}else {
			System.out.println("���ӳ�");
			uo.addUser(u);
//			request.getRequestDispatcher("uselist.jsp").forward(request, response);
			response.sendRedirect("uselist.jsp");
		
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
