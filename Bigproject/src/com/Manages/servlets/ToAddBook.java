package com.Manages.servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.Manages.dao.BaseDao;
import com.Manages.dao.BookOperation;
import com.Manages.entity.Book;

/**
 * Servlet implementation class ToAddBook
 */
@WebServlet("/ToAddBook")
public class ToAddBook extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ToAddBook() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
//		response.getWriter().append("Served at: ").append(request.getContextPath());
		String b_name=request.getParameter("b_name");	
		String author=request.getParameter("author");
		String category=request.getParameter("category");
		String data=BaseDao.getdate();
		int b_numb=Integer.parseInt(request.getParameter("b_numb"));
		int price=Integer.parseInt(request.getParameter("price"));
		
		Book b = new Book();
		BookOperation bo = new BookOperation();
		b.setAuthor(author);
		b.setB_name(b_name);
		b.setB_numb(b_numb);
		b.setCategory(category);
		b.setPrice(price);
		b.setInput_data(data);
		b.setState(1);
		boolean find = bo.selectBookAdd(b.getB_numb());
		if(find) {
			System.out.println("�����ظ���");
			request.getRequestDispatcher("addBook.jsp").forward(request, response);
		}else {
			bo.addBook(b);
		}	
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
