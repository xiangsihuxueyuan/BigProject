package com.Manages.servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.Manages.entity.Lend;
import com.Manages.entity.User;
import com.Manages.service.UserOpService;

/**
 * Servlet implementation class ShowLendToS
 */
@WebServlet("/ShowLendToS")
public class ShowLendToS extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ShowLendToS() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
//		response.getWriter().append("Served at: ").append(request.getContextPath());
		UserOpService uos = new UserOpService();

		
		HttpSession session = request.getSession();
		 User u = (User)session.getAttribute("user");
		 System.out.println(u);
		 if(u.getIden()==0) {
			 System.out.println("查找学生的学号："+u.getS_numb());
			 List<Lend> list = uos.findLendbyu_name(u.getS_numb());
			 request.setAttribute("list", list);
			 request.getRequestDispatcher("booklend.jsp").forward(request, response);
		 }else {
			 List<Lend> list = uos.findLend();
			 request.setAttribute("list", list);
			 request.getRequestDispatcher("booklendM.jsp").forward(request, response);
		 }
		
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
