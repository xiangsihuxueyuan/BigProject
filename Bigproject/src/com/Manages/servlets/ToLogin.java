package com.Manages.servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.Manages.entity.User;
import com.Manages.service.UserOpService;

/**
 * Servlet implementation class ToLogin
 */
@WebServlet("/ToLogin")
public class ToLogin extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ToLogin() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
//		response.getWriter().append("Served at: ").append(request.getContextPath());
		String user_name=request.getParameter("user_name");
		String user_password=request.getParameter("user_password");
		
		User u = new User();
		u.setUser_name(user_name);
		u.setUser_password(user_password);
		
		UserOpService uo = new UserOpService();
		boolean b = uo.selectUser(u);
		if(b) {
			u = uo.selectUserAdd(u);
			HttpSession session = request.getSession();
			session.setAttribute("user", u);
//			session.getAttribute("username");
			request.getRequestDispatcher("ToShowB").forward(request, response);
		}else {
			request.getRequestDispatcher("login.jsp").forward(request, response);
		}
		
//		response.sendRedirect("login.jsp")
		  
			
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
