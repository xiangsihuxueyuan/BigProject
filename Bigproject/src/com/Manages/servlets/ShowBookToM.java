package com.Manages.servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.Manages.entity.Book;
import com.Manages.entity.Page;
import com.Manages.service.BookShowService;

/**
 * Servlet implementation class ShowBookToM
 */
@WebServlet("/ShowBookToM")
public class ShowBookToM extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ShowBookToM() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
//		response.getWriter().append("Served at: ").append(request.getContextPath());
		String pageNow = request.getParameter("pageNow");
		int nowPage=0;
		System.out.println(pageNow);
		if(pageNow==null || pageNow.equals("")){
			nowPage=1;
			System.out.println("for"+nowPage);
		}else{ 
			nowPage=Integer.parseInt(pageNow);
		}
		
		BookShowService bs=new BookShowService();
		Page p = new Page();
		//��ҳ��
		p.setZy(bs.findBookCount());
		p.setPageNum(nowPage);
		
		
		List<Book> list =bs.findBookAll(nowPage, 12);
		request.setAttribute("page", p);
		request.setAttribute("book", list);
		request.getRequestDispatcher("showbookM.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
