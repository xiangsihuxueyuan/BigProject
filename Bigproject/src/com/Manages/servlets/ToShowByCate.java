package com.Manages.servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.Manages.entity.Book;
import com.Manages.entity.Page;
import com.Manages.service.BookShowService;

/**
 * Servlet implementation class ToShowByCate
 */
@WebServlet("/ToShowByCate")
public class ToShowByCate extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ToShowByCate() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
//		response.getWriter().append("Served at: ").append(request.getContextPath());
		String times=request.getParameter("times");
		String category=request.getParameter("category");

		 int nowPage=0;
		 if(times.equals("1")){
        	  nowPage=1;
         }else{
        	  nowPage=Integer.parseInt(request.getParameter("pageNow"));
         }
         BookShowService bs=new BookShowService();
         
         Page p = new Page();
 		//��ҳ��
 		p.setZy(bs.findBookCountByCata(category));
 		p.setPageNum(nowPage);
		
 		List<Book> list = bs.findBookByCategory(category, nowPage, 12);
 		request.setAttribute("category", category);
 		request.setAttribute("page", p);
		request.setAttribute("book", list);
		request.getRequestDispatcher("selectbookbycate.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
