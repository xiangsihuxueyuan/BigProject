package com.Manages.servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.Manages.dao.BaseDao;
import com.Manages.dao.BookOperation;
import com.Manages.entity.Book;
import com.Manages.entity.Lend;
import com.Manages.service.BookOpService;
import com.Manages.service.UserOpService;

/**
 * Servlet implementation class LendBook
 */
@WebServlet("/LendBook")
public class LendBook extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LendBook() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
//		response.getWriter().append("Served at: ").append(request.getContextPath());
		int b_numb=Integer.parseInt(request.getParameter("b_numb"));
		int s_numb=Integer.parseInt(request.getParameter("s_numb"));
		BookOpService bo = new BookOpService();
		Book book = bo.selectBookbq(b_numb);
		UserOpService uo= new UserOpService();
		System.out.println(bo.selectBookAdd(b_numb));
		System.out.println(book.getState()==1);
		System.out.println(uo.selectUserByNumb(s_numb));
		
		if(bo.selectBookAdd(b_numb) && book.getState()==1 && uo.selectUserByNumb(s_numb)) {
			Lend l = new Lend();
			l.setB_numb(b_numb);
			l.setS_numb(s_numb);
			l.setLend_data(BaseDao.getdate());
			l.setLate_data(BaseDao.latedata());
			
			boolean b =bo.addLend(l,0);
			if(b) {
				request.getRequestDispatcher("index.jsp").forward(request, response);
			}else{
				request.getRequestDispatcher("lendbook.jsp").forward(request, response);
			}
		}else {
			request.getRequestDispatcher("lendbook.jsp").forward(request, response);
		}		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
